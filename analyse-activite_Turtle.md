- Objectifs

  apprendre à écrire en Python les instructions conditionnelles, les fonctions et les boucles.

- pré-requis à cette activité

  Je verrai un double objectif:
   * revoir les connaissances en algorithmique
   * la notion de langage de programmation et le langage Python


- Durée de l'activité

  5 à 6  heures

- Exercices cibles
  
- Description du déroulement de l'activité

  * Première page de l'activité à donner aux élèves et les laisser travailler de façon autonome.
  * faire une première synthèse sur les exercices proposés en première page
  * Deuxième page donnée aux élèves
  * faire une deuxième synthèse.

- Anticipation des difficultés des élèves

  proposer des exercices traitant de programmes de calculs et de construction.

- Gestion de l'hétérogénéïté

  cibler des exercices à traiter et d'autres optionnels.
